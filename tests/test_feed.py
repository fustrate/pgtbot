import asyncio
import unittest

from pgtbot.feed import Feed
from pgtbot.feed import Publisher
from pgtbot.feed import Subscriber


loop = asyncio.get_event_loop()


class TestPublisher(unittest.TestCase):

    def test(self):
        async def _test():
            q = asyncio.Queue()
            pub = Publisher(q, lambda: 42)
            await pub._publish()
            return await q.get()

        result = loop.run_until_complete(_test())
        self.assertEqual(result, 42)


class TestSubscriber(unittest.TestCase):

    def test(self):
        async def _test():
            result = []
            sub = Subscriber(lambda x: result.append(x ** 2))
            for i in range(4):
                await sub.queue.put(i)
            for i in range(4):
                await sub._subscribe()
            return result

        result = loop.run_until_complete(_test())
        self.assertListEqual(result, [0, 1, 4, 9])


class TestFeed(unittest.TestCase):

    def test(self):
        result = []

        feed = Feed()
        feed.add_publisher(lambda: 42)
        self.assertEqual(len(feed.publishers), 1)
        self.assertIsInstance(feed.publishers[0], Publisher)
        feed.add_subscriber(lambda x: result.append(x ** 2))
        self.assertEqual(len(feed.subscribers), 1)
        self.assertIsInstance(feed.subscribers[0], Subscriber)

        async def _test():
            await feed.publishers[0]._publish()
            await feed._distribute()
            await feed.subscribers[0]._subscribe()

        loop.run_until_complete(_test())

        expected = [42 * 42]
        self.assertListEqual(result, expected)


if __name__ == "__main__":
    unittest.main()
