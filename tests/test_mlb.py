import unittest

from pgtbot import mlb


class TestSchedule(unittest.TestCase):

    def test(self):
        client = mlb.StatsAPIClient()
        schedule = client.get_schedule(sportId=1, teamId=119, date="2019-07-02")
        self.assertIsInstance(schedule, dict)
        self.assertEqual(len(schedule["dates"]), 1)
        self.assertEqual(len(schedule["dates"][0]["games"]), 1)
        self.assertEqual(schedule["dates"][0]["games"][0]["gamePk"], 565842)

    def test_world_series(self):
        client = mlb.StatsAPIClient()
        games = client.world_series(2018)
        self.assertIsInstance(games, tuple)
        self.assertIsInstance(games[0], mlb.Game)
        self.assertIsInstance(games[0].venue, mlb.Venue)
        self.assertIsInstance(games[0].home_team, mlb.Team)
        self.assertIsInstance(games[0].away_team, mlb.Team)
        self.assertEqual(len(games), 5)

    def test_spring_training(self):
        client = mlb.StatsAPIClient()

        games = client.spring_training(2019, 119)
        self.assertIsInstance(games, tuple)
        self.assertIsInstance(games[0], mlb.Game)
        self.assertIsInstance(games[0].venue, mlb.Venue)
        self.assertIsInstance(games[0].home_team, mlb.Team)
        self.assertIsInstance(games[0].away_team, mlb.Team)
        self.assertEqual(len(games), 32)

        games = client.spring_training(2020, 119)
        self.assertIsInstance(games, tuple)
        self.assertIsInstance(games[0], mlb.Game)
        self.assertIsInstance(games[0].venue, mlb.Venue)
        self.assertIsInstance(games[0].home_team, mlb.Team)
        self.assertIsInstance(games[0].away_team, mlb.Team)
        self.assertEqual(len(games), 33)


class TestTeam(unittest.TestCase):

    def test(self):
        client = mlb.StatsAPIClient()
        api_repr = client.get_team(119)
        self.assertIsInstance(api_repr, dict)
        self.assertEqual(len(api_repr["teams"]), 1)
        team = api_repr["teams"][0]
        self.assertIn("name", team)
        self.assertIn("abbreviation", team)


class TestEventScraper(unittest.TestCase):

    def test(self):
        scraper = mlb.EventScraper()
        events = scraper.events()
        self.assertIsInstance(events, tuple)
        self.assertIsInstance(events[0], mlb.Event)


if __name__ == "__main__":
    unittest.main()
