from pathlib import Path
import re
import unittest

import pendulum

from pgtbot import post


class TestNextSeason(unittest.TestCase):

    def test(self):
        season = post._season(pendulum.date(2019, 1, 1))
        self.assertEqual(season, 2019)
        season = post._season(pendulum.date(2019, 11, 1))
        self.assertEqual(season, 2020)
        season = post._season(pendulum.date(2019, 7, 1))
        self.assertEqual(season, 2019)


class TestQuestions(unittest.TestCase):

    def test(self):
        questions = post.read_discussion_questions()
        self.assertIsInstance(questions, tuple)
        for q in questions:
            self.assertIsInstance(q, str)


class TestOffseasonDiscussion(unittest.TestCase):

    _TEMPLATE = (Path(__file__).parent.parent.absolute() / "templates" / "offseason_daily_discussion.md").read_text()

    def test(self):
        pattern = self._TEMPLATE.format(
            days_since_ws=r"[\d]{1,3}",
            days_until_st=r"[\d]{1,3}",
            team_name="Dodgers",
            next_season=2020,
            question_0=r"[^\n]+",
            question_1=r"[^\n]+",
            question_2=r"[^\n]+",
            markov_question=r"[^\n]+",
            event_0_date=r"[^\n]+",
            event_0_name=r"[^\n]+",
            event_1_date=r"[^\n]+",
            event_1_name=r"[^\n]+",
            event_2_date=r"[^\n]+",
            event_2_name=r"[^\n]+",
        ).replace("*", "[*]")
        odt = post.OffseasonDiscussion(pendulum.date(2019, 11, 14), 119)
        self.assertTrue(re.match(pattern, str(odt)))


if __name__ == "__main__":
    unittest.main()
