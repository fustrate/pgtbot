import datetime as dt
from pathlib import Path
import unittest

import pendulum

from pgtbot import dispatch


dispatch._dbname = "pgtbot_test.db"


class TestOffseason(unittest.TestCase):

    def tearDown(self):
        Path(dispatch._dbname).unlink()

    def test(self):
        result = dispatch.offseason(dt.datetime(2019, 1, 1, 0))
        self.assertIsNone(result)
        result = dispatch.offseason(dt.datetime(2019, 1, 1, 15))
        self.assertIsInstance(result, dt.date)
        result = dispatch.offseason(dt.datetime(2019, 1, 1, 15))
        self.assertIsNone(result)
        result = dispatch.offseason(dt.datetime(2019, 1, 1, 18))
        self.assertIsNone(result)
        result = dispatch.offseason(dt.datetime(2019, 1, 2, 1))
        self.assertIsNone(result)
        result = dispatch.offseason(dt.datetime(2019, 1, 2, 15))
        self.assertIsInstance(result, dt.date)


if __name__ == "__main__":
    unittest.main()
