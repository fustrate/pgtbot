# PGT Bot

Automated Game Day, Off Day, and Post Game threads for a baseball subreddit.

[![pipeline status](https://gitlab.com/dtrox/pgtbot/badges/master/pipeline.svg)](https://gitlab.com/dtrox/pgtbot/commits/master)
[![coverage report](https://gitlab.com/dtrox/pgtbot/badges/master/coverage.svg)](https://gitlab.com/dtrox/pgtbot/commits/master)
