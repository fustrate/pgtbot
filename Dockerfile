FROM python:3.8-slim

RUN pip install pipenv

WORKDIR /usr/src/pgtbot

COPY Pipfile Pipfile.lock setup.py README.md ./
COPY pgtbot ./pgtbot
COPY data ./data
COPY templates ./templates

RUN pipenv install --system --deploy

WORKDIR /pgtbot

ENTRYPOINT [ "python", "-u", "/usr/src/pgtbot/pgtbot" ]
CMD [ "--help" ]
