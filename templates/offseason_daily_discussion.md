# Offseason Daily Discussion Thread

Welcome to **day {days_since_ws}** of the offseason. Only **{days_until_st} days** until the {team_name}' first Spring Training game of {next_season}!

### Conversation starters for today:

1. {question_0}
2. {question_1}
3. {question_2}
4. {markov_question}

### Upcoming MLB Events:

| Date | Event |
|:----:|:-----:|
| {event_0_date} | {event_0_name} |
| {event_1_date} | {event_1_name} |
| {event_2_date} | {event_2_name} |

Have a great day, r/Dodgers.
