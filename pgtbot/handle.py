import logging

from praw import Reddit
import pendulum

from pgtbot import post
from pgtbot.util import Config
from pgtbot import log


def offseason(
    event: pendulum.DateTime, reddit: Reddit, config: Config,
):
    thread = post.OffseasonDiscussion(
        event.in_tz("America/New_York").date(), config.mlb_team_id
    )
    flair_id = config.flair_ids.get("offseason")

    submission = reddit.subreddit(config.subreddit).submit(
        thread.title, selftext=str(thread), flair_id=flair_id, send_replies=False
    )
    submission.mod.sticky()

    logger = log.JSONLogger(logging.getLogger(__name__))
    logger.info("Posted offseason thread to subreddit.", permalink=submission.permalink)
