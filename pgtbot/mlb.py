from dataclasses import dataclass
from functools import lru_cache
import threading
from typing import Dict, List, Tuple

import bs4
import pendulum
import requests

from pgtbot.__version__ import __version__


_local = threading.local()


def _session():
    try:
        return _local.session
    except AttributeError:
        _local.session = requests.Session()
        _local.session.headers.update(
            {"User-Agent": "pgtbot/{__version__} (+https://gitlab.com/dtrox/pgtbot)"}
        )
        return _local.session


@dataclass
class Venue(object):

    id: int
    name: str
    city: str
    state: str
    coordinates: Tuple[float]

    @classmethod
    def from_api_repr(cls, venue: Dict):
        return cls(
            id=venue["id"],
            name=venue["name"],
            city=venue["location"]["city"],
            state=venue["location"]["state"],
            coordinates=(
                venue["location"]["defaultCoordinates"]["latitude"],
                venue["location"]["defaultCoordinates"]["longitude"],
            ),
        )


@dataclass
class Team(object):

    id: int
    name: str
    team_code: str
    abbreviation: str
    team_name: str
    location_name: str
    sport: str
    league: str
    division: str

    @classmethod
    def from_api_repr(cls, team: Dict):
        return cls(
            id=team["id"],
            name=team["name"],
            team_code=team["teamCode"],
            abbreviation=team["abbreviation"],
            team_name=team["teamName"],
            location_name=team["locationName"],
            sport=team["sport"]["name"],
            league=team["league"]["name"],
            division=team["division"]["name"],
        )


@dataclass
class Game(object):

    game_pk: int
    game_type: str
    season: int
    game_date: pendulum.DateTime
    status_code: str
    venue: Venue
    away_team: Team
    away_score: int
    home_team: Team
    home_score: int
    games_in_series: int
    series_game_number: int

    @classmethod
    def from_api_repr(cls, game: Dict):
        venue = Venue.from_api_repr(game["venue"])
        away_team = Team.from_api_repr(game["teams"]["away"]["team"])
        home_team = Team.from_api_repr(game["teams"]["home"]["team"])
        return cls(
            game_pk=game["gamePk"],
            game_type=game["gameType"],
            season=int(game["season"]),
            game_date=pendulum.parse(game["gameDate"]),
            status_code=game["status"]["statusCode"],
            venue=venue,
            away_team=away_team,
            away_score=game["teams"]["away"].get("score"),
            home_team=home_team,
            home_score=game["teams"]["home"].get("score"),
            games_in_series=game["gamesInSeries"],
            series_game_number=game["seriesGameNumber"],
        )


class StatsAPIClient(object):
    def __init__(self):
        self.session = _session()

    @lru_cache()
    def get_schedule(self, **params) -> List[dict]:
        url = "https://statsapi.mlb.com/api/v1/schedule"
        response = self.session.get(url, params=params)
        response.raise_for_status()
        return response.json()

    @lru_cache()
    def get_team(self, team_id: int, **params) -> List[dict]:
        url = f"https://statsapi.mlb.com/api/v1/teams/{team_id}"
        response = self.session.get(url, params=params)
        response.raise_for_status()
        return response.json()

    @lru_cache()
    def get_gumbo(self, game_pk: int) -> dict:
        url = f"https://statsapi.mlb.com/api/v1.1/game/{game_pk}/feed/live"
        response = self.session.get(url)
        response.raise_for_status()
        return response.json()

    @lru_cache()
    def get_content(self, game_pk: int) -> dict:
        url = f"https://statsapi.mlb.com/api/v1/game/{game_pk}/content"
        response = self.session.get(url)
        response.raise_for_status()
        return response.json()

    def world_series(self, season: int) -> Tuple[Game]:
        schedule = self.get_schedule(
            startDate=f"{season}-10-10",
            endDate=f"{season}-11-15",
            sportId=1,
            gameType="W",
            hydrate="venue(location),team",
        )

        raw_games = (g for d in schedule["dates"] for g in d["games"])
        games = map(Game.from_api_repr, raw_games)

        return tuple(games)

    def spring_training(self, season: int, team_id: int) -> Tuple[Game]:
        schedule = self.get_schedule(
            startDate=f"{season}-02-15",
            endDate=f"{season}-04-05",
            sportId=1,
            teamId=team_id,
            gameType="S",
            hydrate="venue(location),team",
        )

        raw_games = (g for d in schedule["dates"] for g in d["games"])
        games = map(Game.from_api_repr, raw_games)

        return tuple(games)

    def team(self, team_id: int) -> Team:
        team = self.get_team(team_id)["teams"][0]
        return Team.from_api_repr(team)


@dataclass
class Event(object):

    date: str
    name: str


class EventScraper(object):
    def __init__(self):
        self.session = _session()

    def events(self):
        url = "https://www.mlb.com/schedule/events"

        response = self.session.get(url)
        response.raise_for_status()

        soup = bs4.BeautifulSoup(response.text, features="html.parser")
        url = soup.find("a", string="Events - Important Dates").get("href")

        response = self.session.get(url)
        response.raise_for_status()

        soup = bs4.BeautifulSoup(response.text, features="html.parser")
        tds = soup.find("table").find_all("td")
        values = (e.get_text() for e in tds)

        raw_events = zip(*[values] * 2)
        events = []
        for date, name in raw_events:
            events.append(Event(date=date, name=name))

        return tuple(events)
