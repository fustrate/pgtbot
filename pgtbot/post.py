from functools import lru_cache
from pathlib import Path
import random
import markovify
from typing import Dict, Iterable, Tuple

import pendulum

from pgtbot import mlb


def _season(date: pendulum.Date):
    """Year of the current or upcoming baseball season."""
    if date.month > 10:
        return date.year + 1
    return date.year


@lru_cache()
def read_discussion_questions():
    path = Path(__file__).parent.parent.absolute() / "data" / "questions.txt"
    with open(path, encoding="utf-8") as fobj:
        questions = tuple(fobj.read().splitlines())
    return questions


def markov_question():
    path = Path(__file__).parent.parent.absolute() / "data" / "questions.txt"
    with open(path, encoding="utf-8") as fobj:
        return markovify.NewlineText(fobj.read(), state_size=3).make_sentence()


class Post(object):

    template: str
    attributes: Dict[str, str]
    title: str

    def __str__(self):
        return self.template.format(**self.attributes)


class OffseasonDiscussion(Post):

    template: str = (
        Path(__file__).parent.parent.absolute()
        / "templates"
        / "offseason_daily_discussion.md"
    ).read_text()

    def __init__(self, date: pendulum.Date, team_id: int):
        next_season = _season(date)

        client = mlb.StatsAPIClient()
        world_series = client.world_series(next_season - 1)
        spring_training = client.spring_training(next_season, team_id)
        team = client.team(team_id)

        days_since_ws = (
            date - world_series[-1].game_date.in_tz("Pacific/Pago_Pago").date()
        ).days
        days_until_st = (
            spring_training[0].game_date.in_tz("Pacific/Pago_Pago").date() - date
        ).days

        questions = random.sample(read_discussion_questions(), 3)

        events = mlb.EventScraper().events()

        self.title = f"Offseason Daily Thread ({date.to_formatted_date_string()}) - General Discussion"
        self.attributes = {
            "days_since_ws": days_since_ws,
            "days_until_st": days_until_st,
            "team_name": team.team_name,
            "next_season": next_season,
            "markov_question": markov_question(),
            **{f"question_{i}": q for i, q in enumerate(questions)},
            **{f"event_{i}_date": e.date for i, e in enumerate(events)},
            **{f"event_{i}_name": e.name for i, e in enumerate(events)},
        }
