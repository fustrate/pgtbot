import json
import logging


class JSONLogger(object):
    def __init__(self, logger: logging.Logger):
        self.logger = logger

    def log(self, level: int, msg: str, **kwargs):
        log_msg = json.dumps({"msg": msg, **kwargs})

        if level == logging.ERROR:
            stack_info = True
            exc_info = True
        else:
            stack_info = False
            exc_info = False

        self.logger.log(level, log_msg, stack_info=stack_info, exc_info=exc_info)

    def info(self, msg, **kwargs):
        self.log(logging.INFO, msg, **kwargs)
