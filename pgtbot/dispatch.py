import logging
import sqlite3

import pendulum

from pgtbot import log


_dbname = "pgtbot.db"


def offseason(datetime: pendulum.DateTime = None) -> pendulum.DateTime:
    datetime = datetime or pendulum.now(tz="utc")
    if datetime.time() < pendulum.time(15):
        return None
    conn = sqlite3.connect(_dbname)
    with conn:
        conn.execute(
            "create table if not exists dispatch_offseason ( date text primary key );"
        )
        cursor = conn.execute(
            "select date from dispatch_offseason where date=?;",
            (datetime.date().isoformat(),),
        )
        result = cursor.fetchone()
        if result is None:
            logger = log.JSONLogger(logging.getLogger(__name__))
            logger.info("Dispatching offseason event.", event=datetime.isoformat())
            conn.execute(
                "insert into dispatch_offseason values ( ? );",
                (datetime.date().isoformat(),),
            )
            return datetime
