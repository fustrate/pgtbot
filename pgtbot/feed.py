import asyncio
import collections
import logging
from typing import Callable, List

from pgtbot import log


def _dispatch_wrapper(func: Callable):
    try:
        return func()
    except Exception as err:
        logger = log.JSONLogger(logging.getLogger(__name__))
        logger.log(
            logging.ERROR,
            "Error occurred during thread run.",
            err=err.__class__.__name__,
            err_msg=str(err),
        )
        return None


class Publisher(object):
    def __init__(self, queue: asyncio.Queue, dispatcher: Callable, delay: int = 1):
        self.queue = queue
        self.dispatcher = dispatcher
        self.delay = delay

    async def _publish(self):
        event = _dispatch_wrapper(self.dispatcher)
        if event is not None:
            await self.queue.put(event)

    async def publish(self):
        while True:
            await self._publish()
            await asyncio.sleep(1)


def _handle_wrapper(func: Callable, event):
    try:
        return func(event)
    except Exception as err:
        logger = log.JSONLogger(logging.getLogger(__name__))
        logger.log(
            logging.ERROR,
            "Error occurred during thread run.",
            err=err.__class__.__name__,
            err_msg=str(err),
        )
        return None


class Subscriber(object):
    def __init__(self, handler: Callable, delay: int = 1):
        self.queue = asyncio.Queue()
        self.handler = handler
        self.delay = delay

    async def _subscribe(self):
        event = await self.queue.get()
        if event is not None:
            _handle_wrapper(self.handler, event)

    async def subscribe(self):
        while True:
            await self._subscribe()
            await asyncio.sleep(1)


class Feed(object):
    def __init__(self, delay: int = 1):
        self.queue = asyncio.Queue()
        self.delay = delay
        self.publishers = []
        self.subscribers = []

    def add_publisher(self, dispatcher: Callable):
        self.publishers.append(Publisher(self.queue, dispatcher, delay=self.delay))

    def add_subscriber(self, handler: Callable):
        self.subscribers.append(Subscriber(handler, delay=self.delay))

    async def _distribute(self):
        event = await self.queue.get()
        if event is not None:
            for subscriber in self.subscribers:
                await subscriber.queue.put(event)

    async def distribute(self):
        while True:
            await self._distribute()
            await asyncio.sleep(self.delay)

    async def run(self):
        publishers = [p.publish() for p in self.publishers]
        subscribers = [s.subscribe() for s in self.subscribers]
        feed = self.distribute()
        await asyncio.gather(*publishers, *subscribers, feed)
