import asyncio
import argparse
import functools
import logging
from pathlib import Path
from typing import List, Tuple

import praw
from ruamel.yaml import YAML

from pgtbot import dispatch
from pgtbot.feed import Feed
from pgtbot import handle
from pgtbot.util import Config


yaml = YAML(typ="safe")


def _default_config_path() -> str:
    path = Path.cwd() / ".pgtbot" / "config.yaml"
    if path.exists():
        return path

    path = Path.home() / ".pgtbot" / "config.yaml"
    if path.exists():
        return path

    raise ValueError(
        "No config found. Either place a config in `.pgtbot/config.yaml` or specify a path with `--config`."
    )


def configure(args: List[str] = None) -> Tuple[Config]:
    parser = argparse.ArgumentParser()
    parser.add_argument("--config")
    args = parser.parse_args(args)

    config = args.config or _default_config_path()

    subreddits = yaml.load(Path(config).read_text())["subreddits"]

    return tuple(Config(subreddit=name, **cfg) for name, cfg in subreddits.items())


def main():
    logging.basicConfig(level=logging.INFO, force=True)

    configs = configure()

    print("=== PGTBot ===")

    loop = asyncio.get_event_loop()

    offseason_feed = Feed()
    offseason_feed.add_publisher(dispatch.offseason)
    for config in configs:
        reddit = praw.Reddit(config.praw_bot)
        if "offseason" in config.threads:
            handler = functools.partial(handle.offseason, reddit=reddit, config=config)
            offseason_feed.add_subscriber(handler)

    loop.run_until_complete(asyncio.gather(offseason_feed.run()))


if __name__ == "__main__":
    main()
