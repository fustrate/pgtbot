import dataclasses
from typing import Dict, List


@dataclasses.dataclass
class Config(object):

    subreddit: str
    praw_bot: str
    mlb_team_id: int
    threads: List[str]
    flair_ids: Dict[str, str]
