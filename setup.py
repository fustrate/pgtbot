from pathlib import Path
import setuptools


_here = Path(__file__).parent.absolute()


with open(_here / 'README.md', encoding='utf-8') as fobj:
    long_description = fobj.read()


_requires = ["praw", "requests", "pytz"]


setuptools.setup(
    name='pgtbot',
    version='2.1.1',
    description='Baseball subreddit daily discussion thread automation.',
    long_description=long_description,
    long_description_content_type='text/markdown',
    url='https://gitlab.com/dtrox/pgtbot',
    author='Drew Troxell',
    author_email='code@trox.space',
    packages=setuptools.find_packages(where='pgtbot'),
    python_requires='>=3.7, <4',
    install_requires=[_requires],
)
